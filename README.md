# Logbook UpSurge

## 27/04
Finishing implementing the interactive app with mores features
Adding groups machines to the control network
Begining of project report and demonstration


## 20/03
Research on interactive app in Rust
Research on how to implement a interactive app in Rust 
Begining of the implementation of the interactive app
Control Network V1 fonctionnal
Testing the control network


## 13/03
Research on control Network and how to implement it
Begining of the implementation of the control network


## 06/03
Refactoring project structure from scratch
Conception of a new project architecture


## 27/02
Preparing presentation
Imbricate SSH test : Creating a SSH connection from a machine B to a server through a SSH connection from A to B.

Brainstorming Deployement method :
 - Download an executable on the distant machine and run it (recursive)
 - Pre-process all the commands for all distant machine on a single string (imbricate SSH) and execute it on the root machine 
 Questions : How the feedback of each execution is send to the root machine ? Backpropagation (Retro-propagation) with a .txt file ? 


## 20/02
Explaining code already written to other members of the team
Discussing and designing software architecture


## 13/02 (vacation)
Commenting existing code
Creating git issues


## 6/02
Discovering and learning Rust
Discussing use of ssh or grpc with teacher
Implementation of a client/server application using ssh in Rust
Implementaion of a basic project structure


## 30/01
Discovering and learning Rust
Discovery of tokio, tonic, grpc
Test SSH in Rust : implementation of a rust program which create a SSH connection between a computer and a server. 


## 23/01
Setting up project management strategies
Discovering and learning Rust
Exploring and discussing ssh librabries in Rust


## 16/01
Discovering and learning Rust
Exploring TakTuk's repository


## 9/01
Presentation of the topics 
Creation of the group
Choice of the subject
